<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script>window.Laravel = { csrfToken: '{{ csrf_token() }}' }</script>
        <title>Larvel Vue</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <style>
            ul.nav {
                margin-bottom: 50px!important;
            }
            .router-link-exact-active {
                background-color: #37a3f133;
                border-radius: 3px;
            }
        </style>
    </head>
    <body>
        <div id="app">
            <div class="container">
                <!-- <navigation-app/> -->
               
                    <ul class="nav">
                        <li class="nav-item">
                            <router-link class="nav-link" to="/">Home</router-link>
                        </li>
                        <li class="nav-item">
                            <router-link class="nav-link" to="/tag">Tag</router-link>
                        </li>
                        <li class="nav-item">
                            <router-link class="nav-link" to="/image">Image</router-link>
                        </li>
                    </ul>
                <!-- <img src="{{asset('storage/images/KFYfK8fTi3i9LvmDWKd9iykRImEeVj1uMy3gLojW.png')}}" alt=""> -->
                <router-view></router-view>
            </div>
        </div>
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
