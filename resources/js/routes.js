import Index from './pages/Index.vue';
import Tag from './pages/tag/Index.vue';
import Image from './pages/image/Index.vue';

const routes = [
  {path: '/', component: Index},
  {path: '/tag', component: Tag},
  {path: '/image', component: Image},
]

export default routes;